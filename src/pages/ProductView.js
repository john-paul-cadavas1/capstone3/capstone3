import { useState, useEffect, useContext } from 'react'
import { Container, Card, Row, Col, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user, cartItems } = useContext(UserContext);

	const navigate = useNavigate();

	
	const { productId } = useParams()

	const [imgSrc, setImgSrc] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	function addQuantity () {
            setQuantity(quantity + 1);
        }

    function subtractQuantity () {
            setQuantity(quantity - 1);
        }

    function onAdd () {
    	fetch(`http://localhost:4000/products/${productId}`)
		.then(res=> res.json())
		.then(data=> {
			//console.log(data)
			cartItems.push(data)

			Swal.fire({
				title: "Success",
				icon: "success",
				text: "Added to cart"
			})
		})
    	
    	}


	useEffect(()=> {
		console.log(productId)
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res=> res.json())
		.then(data=> {
			//console.log(data)

			setImgSrc(data.imgSrc)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [productId])

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span:6, offset: 3}}>
					<Card>
						<Card.Img className="cardview-image" variant="top" src={imgSrc} />
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							
							{
								(user.id !== null) ?
											      	
									                
											        
									<Button onClick={() => onAdd()} variant="info">Add to Cart</Button>
									:
									<Link className="btn btn-danger" to="/login">Login to Buy</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}