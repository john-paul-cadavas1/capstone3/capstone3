import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'




export default function Signup() {

	const navigate = useNavigate();
	const { user } = useContext(UserContext)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [isActive, setIsActive] = useState(false);


		function registerUser(e) {

			e.preventDefault()

			fetch('http://localhost:4000/users/checkEmail', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email:email
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data) {
					Swal.fire({
	                        title: "Duplicate Email Found",
	                        icon: "error",
	                        text: "Please provide a different email"
	                     }) 
				} else {
					fetch('http://localhost:4000/users/register', {
								method: 'POST',
								headers: {
									'Content-Type': 'application/json'
								},
								body: JSON.stringify({
									firstName: firstName,
									lastName: lastName,
									email: email,
									password: password1,
									mobileNo: mobileNumber
								})
							})
							.then(res => res.json())
							.then(data => {

								if(data) {
									Swal.fire({
									    title: "Success",
									    icon: "success",
									    text: "You have successfully registered"
									})

									setFirstName('');
									setLastName('');
									setEmail('');
									setPassword1('');
									setPassword2('');
									setMobileNumber('');

									navigate('/login')
								} else {
									Swal.fire({
					                        title: "Failed",
					                        icon: "error",
					                        text: "Check your details and try again!"
					                     }) 
								}
							})
							

				}
			})

		}

		useEffect(() => {

			if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNumber !== '') && (mobileNumber.length === 11) && (password1 === password2)) {
				setIsActive(true);
			} else {
				setIsActive(false)
			}

		}, [firstName, lastName, email, password1, password2, mobileNumber])


	return(

		(user.id !== null) ? 

			<Navigate to="/login"/>
			
			:
			<Form className="signup-form border border-solid mt-4" onSubmit = {(e) => registerUser(e)} class>
			<h2>Signup</h2>
		  	<Form.Group className="mb-3" controlId="firstName">
		    	<Form.Label>First Name</Form.Label>
		    		<Form.Control 
		    				type="text" 
		    				placeholder="Enter First Name"
		    				value = {firstName}
		    				onChange = {e => setFirstName(e.target.value)}
		    				required />
		  	</Form.Group>
		  	<Form.Group className="mb-3" controlId="lastName">
		    	<Form.Label>Last Name</Form.Label>
		    		<Form.Control 
		    				type="text" 
		    				placeholder="Enter Last Name"
		    				value = {lastName}
		    				onChange = {e => setLastName(e.target.value)}
		    				required />
		  	</Form.Group>
		  	<Form.Group className="mb-3" controlId="userEmail">
		    	<Form.Label>Email address</Form.Label>
		    		<Form.Control 
		    				type="email" 
		    				placeholder="Enter email"
		    				value = {email}
		    				onChange = {e => setEmail(e.target.value)}
		    				required />
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="password1">
		    	<Form.Label>Password</Form.Label>
		   	 		<Form.Control 
		    				type="password" 
		    				placeholder="Password"
		    				value={password1}
		    				onChange= {e => setPassword1(e.target.value)}
		    				required />
		  	</Form.Group>

		  	<Form.Group className="mb-3" controlId="password2">
		    	<Form.Label>Verify Password</Form.Label>
		    		<Form.Control 
		    				type="password" 
		    				placeholder="Verify Password"
		    				value={password2}
		    				onChange= {e => setPassword2(e.target.value)}
		    				required />
		  </Form.Group>

		  	<Form.Group className="mb-3" controlId="mobileNumber">
		   	 <Form.Label>Mobile Number</Form.Label>
		    	<Form.Control 
		    				type="text" 
		    				placeholder="Enter your mobile number"
		    				value={mobileNumber}
		    				onChange={e=> setMobileNumber(e.target.value)}
		    				required />
		  	</Form.Group>

			{ isActive ? 
			    	<Button className=" btn-info btn-block login-form mt-3" type="submit" id="submitBtn">Signup</Button>
			        : 
			        <Button className=" btn-danger btn-block login-form mt-3" type="submit" id="submitBtn" disabled>Signup</Button>
			}

			<div className="text-center mt-2">
			    	<p>Already have an account? <Link to={`/login`}>Login</Link></p>
			    </div>


		</Form>

		







		)
}