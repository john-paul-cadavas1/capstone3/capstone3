import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import {Fragment, React, useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext';
import ItemCard from '../components/ItemCard';
import styled from 'styled-components';


export default function Cart() {

	const {cartItems, setCartItems, items, setItems, clearCart} = useContext(UserContext);

	// const [items, setItems] =useState([]);

	/*const clearCart = () => {
      
      setCartItems([]);
      setItems([]);
      // const arr = items.filter((item) => item._id !== id);

      // console.log(arr)
      // setItems(arr)

   }*/

	useEffect(() => {
			setItems(cartItems.map(item => {
				return(

					<ItemCard key={item._id} itemProp={item}/>
					
					)
			}))

		}, [])

	const Container = styled.div`

	`

	const Wrapper = styled.div`
	padding: 20px;
	`

	const Tittle = styled.h1`
	font-weight: 300;
	text-align: center;
	`

	const Top = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	`

	const TopButton = styled.button`
	padding: 10px;
	font-weight: 600;
	cursor: pointer;
	`

	const TopTexts = styled.div`

	`

	const TopText = styled.span`
	text-decoration: underline;
	cursor: pointer;
	margin: 0 10px;
	`

	const Bottom = styled.div`
	display: flex;
	justify-content: center;
	`

	const Info = styled.div`
	flex: 3;
	`

	const Summary = styled.div`
	flex: 1;
	border: 0.5px solid lightgray;
	border-radius: 10px;
	padding: 20px;
	height: 50vh;
	margin-top: 4rem;
	`

	const SummaryTittle = styled.h1`
	font-weight: 200;
	`
	const SummaryItem = styled.div`
	margin: 30px 0px;
	display: flex;
	justify-content: space-between;
	`
	const SummaryItemText = styled.span``
	const SummaryItemPrice = styled.span``
	const Button = styled.button`
	width: 100%;
	padding: 10px;
	font-weight: 600;
	background-color: #31D5EA;
	border: none;
	`

	return(

		<Container>
			<Wrapper>
				<Tittle>Your Cart</Tittle>
				<Top>
					
					<TopButton>Continue Shopping</TopButton>
					<TopTexts>
						<TopText>Shopping Bag({cartItems.length})</TopText>
						<TopText>Your Wishlist (0)</TopText>
					</TopTexts>
					<TopButton onClick={() => clearCart()}>Clear Cart</TopButton>
				</Top>
				<Bottom>
					
					<Info>
						{items}
					</Info>
					<Summary>
						<SummaryTittle>ORDER SUMMMARY</SummaryTittle>
						<SummaryItem>
							<SummaryItemText>Subtotal</SummaryItemText>
							<SummaryItemPrice>Php </SummaryItemPrice>
						</SummaryItem>
						<SummaryItem>
							<SummaryItemText>Shipping Fee</SummaryItemText>
							<SummaryItemPrice>Php 200</SummaryItemPrice>
						</SummaryItem>
						<SummaryItem>
							<SummaryItemText>TOTAL</SummaryItemText>
							<SummaryItemPrice>Php </SummaryItemPrice>
						</SummaryItem>
						<Button variant="info">Checkout Now</Button>
					</Summary>
				</Bottom>
			</Wrapper>
		</Container>

		

		)
}

ItemCard.propTypes = {

	productProp: PropTypes.shape({
		imgSrc: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	})
}