import PropTypes from 'prop-types';
import {Fragment, useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';

export default function Products() {

	
	const [products, setProducts] =useState([]);

	

	useEffect(() => {
		fetch("http://localhost:4000/products/")
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {
				return(

					<ProductCard key={product._id} productProp={product} />
					
					)
			}))

		})


	}, [])

	return (

			<Fragment>
				<div className="wrapper mt-5">
					{products}
				</div>
			</Fragment>
		)
}

ProductCard.propTypes = {

	productProp: PropTypes.shape({
		imgSrc: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		stock: PropTypes.number.isRequired
	})
}