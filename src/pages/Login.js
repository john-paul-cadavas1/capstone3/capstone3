import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login(){

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);


	function authenticate(e) {

		e.preventDefault();

		fetch("http://localhost:4000/users/login", {
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email,
		        password: password
		    })

		})
		.then(res => res.json())
		.then(data => {

		    if(typeof data.access !== "undefined") {

		        localStorage.setItem('token', data.access)
		        retrieveUserDetails(data.access)

		        Swal.fire({
		            title: "Login Successful",
		            icon: "success",
		            text: "Welcome to Toys4Boys!"
		        })

		    } else {

		         Swal.fire({
		            title: "Authentication Failed",
		            icon: "error",
		            text: "Check your login details and try again!"
		         })   
		    }


		})
		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) => {

	    fetch('http://localhost:4000/users/details', {
	        headers: {
	            Authorization: `Bearer ${token}`
	        }
	    })
	    .then(res => res.json())
	    .then( data => {

	        setUser({
	            id: data._id,
	            isAdmin: data.isAdmin
	        })

	    })
	}

	useEffect(() => {

           
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);




	return(

		(user.id !== null) ? 

            <Navigate to ="/" />
            :
            	<Form className="login-form mt-5 border border-solid" onSubmit={(e) => authenticate(e)}>
			    <h2 className="mb-3 mt-3 text-center">Login</h2>
			    <Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			            type="email" 
			            placeholder="Enter email"
			            value={email}
			            onChange={(e) => setEmail(e.target.value)}
			
			            required
			        />
			    </Form.Group>

			    <Form.Group className="mb-3 mt-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			            type="password" 
			            placeholder="Password"
			            value={password}
			            onChange={(e) => setPassword(e.target.value)}
			
			            required
			        />
			    </Form.Group>

			    { isActive ? 
			    	<Button className=" btn-info btn-block login-form mt-3" type="submit" id="submitBtn">Log in</Button>
			        : 
			        <Button className=" btn-danger btn-block login-form mt-3" type="submit" id="submitBtn" disabled>Log in</Button>
			    }

			    <div className="text-center mt-1">
			    	<p>Not a member? <Link to={`/signup`}>Signup</Link></p>
			    </div>
			</Form>
			 















		)



}




		