import { useState, useEffect } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap'
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Signup from './pages/Signup';
import Error from './pages/Error';
import Cart from './pages/Cart';
import { UserProvider } from './UserContext';


import './App.css';

function App() {

  const [cartItems, setCartItems] = useState([]);

  const [items, setItems] =useState([]);

  const clearCart = () => {
      
      setCartItems([]);
      setItems([]);
      // const arr = items.filter((item) => item._id !== id);

      // console.log(arr)
      // setItems(arr)

   }

   const removeItem = (id) => {
      console.log(id)
      const arr = cartItems.filter((item) => item._id !== id);
      const arr2 = items.filter((item) => item._id !== id);

      console.log(arr)
      setCartItems(arr)
      setItems(arr2)
      console.log(cartItems)
      /*const pos = cartItems.map(function(e) {return e._id}).indexOf(_id)
      console.log(pos)
      cartItems.slice(pos, 1)
      console.log(cartItems)*/

   }


  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  
  //Function toclear the localstorage for logout
  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect( () => {

    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then( res => res.json())
    .then( data => {

      if(typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })

  }, [])






  return (
    <UserProvider value= {{user, setUser, unsetUser, cartItems, setCartItems, items, setItems, clearCart, removeItem}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route exact path="/" element={<Home/>} />
              <Route exact path="/products" element={<Products/>} />
              <Route exact path="/products/:productId" element={<ProductView/>} />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/logout" element={<Logout/>} />
              <Route exact path="/signup" element={<Signup/>} />
              <Route path="*" element={<Error />} />
              <Route exact path="/cart" element={<Cart/>} />
            </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}

export default App;
