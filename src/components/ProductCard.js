import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, name, description, price, imgSrc, stock } = productProp;


	return(
		    	<Card className="card" style={{ width: '18rem' }}>
		    	  <Card.Img className="card-image" variant="top" src={imgSrc} />
		    	  <Card.Body>
		    	    <Card.Title className="card-tittle">{name}</Card.Title>
		    	    <Card.Subtitle className="card-sibtittle">Description:</Card.Subtitle>
		    	    <Card.Text className="card-sibtittle">{description}</Card.Text>
		    	    <Card.Subtitle className="card-sibtittle">Price:</Card.Subtitle>
		    	    <Card.Text className="card-sibtittle">PhP {price}</Card.Text>
		    	    <Card.Subtitle className="card-sibtittle">Stock:</Card.Subtitle>
		    	    <Card.Text className="card-sibtittle">{stock}</Card.Text>
		    	    <Link className="btn btn-info card-btn" to={`/products/${_id}`}>Buy Now</Link>
		    	  </Card.Body>
		    	</Card>
		





		)
}