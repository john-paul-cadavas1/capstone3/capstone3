import { useState, Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import { Button, Form, Container} from 'react-bootstrap';
import styled from 'styled-components';
import { FaSearch, FaShoppingCart} from 'react-icons/fa';
import UserContext from '../UserContext';


export default function AppNavBar(){

	const { user } = useContext(UserContext);
	const [searchBar, setSearchBar] = useState("");

	return(

		<Navbar className="p-3" bg="info" expand="lg">
		    <Navbar.Brand as={ Link } to="/">Toys4Boys</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      	<Nav className="me-auto">
		      	  <Nav.Link as={ Link } to="/">Home</Nav.Link>
		      	  <Nav.Link as={ Link } to="/products">Products</Nav.Link>
		      	  
		      	</Nav>
		      	<Form className="d-flex m-1">
		        	<Form.Group controlId="searchBar">
                    <Form.Control 
                        type="text" 
                        placeholder="Search"
                        value={searchBar}
                        onChange={(e) => setSearchBar(e.target.value)}
                    />
                </Form.Group>
                <Button variant="secondary"><FaSearch/></Button>
                
		        </Form>
		        <Link className="btn btn-secondary m-1" to={`/cart`}><FaShoppingCart/></Link>

		        {
		        	(user.id !== null ) ?
		        		<Link className="btn btn-secondary m-1" to={`/logout`}>Logout</Link>
		        	: 
		        		<Fragment>
		        			<Link className="btn btn-secondary m-1" to={`/login`}>Login</Link>
		        			<Link className="btn btn-secondary m-1" to={`/signup`}>Signup</Link>
		        		</Fragment>

		        }
		    </Navbar.Collapse>
		</Navbar>
		





		)
}