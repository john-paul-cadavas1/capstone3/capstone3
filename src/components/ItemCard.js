import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {Fragment, useState, useEffect, useContext} from 'react';
import styled from 'styled-components';
import { GrAdd, GrSubtract } from 'react-icons/gr';
import UserContext from '../UserContext';



export default function ProductCard({itemProp}) {

	const [quantity, setQuantity] = useState(1);
	// const [totalAmount, setTotalAmount] = useState(0);
	const { _id, name, description, price, imgSrc } = itemProp;
	const totalAmount = price * quantity
	const {cartItems, setCartItems} = useContext(UserContext);

	// const removeItem = (id) => {
      
 //      const arr = cartItems.filter((item) => item._id !== id);

 //      console.log(arr)
 //      setCartItems(arr)
 //      console.log(cartItems)
 //      /*const pos = cartItems.map(function(e) {return e._id}).indexOf(_id)
 //      console.log(pos)
 //      cartItems.slice(pos, 1)
 //      console.log(cartItems)*/

 //   }

	function addQuantity () {
	        setQuantity(quantity + 1);
	    }

	function subtractQuantity () {
	        setQuantity(quantity - 1);
	    }



	const Product = styled.div`
	display: flex;
	justify-content: space-between;
	margin-top: 4rem;
	`
	const ProductDetail = styled.div`
	flex: 2;
	display: flex;
	`
	const Image = styled.img`
	width: 10rem;
	height: 10rem;
	`
	const Details = styled.div`
	padding: 20px;
	display: flex;
	flex-direction: column;
	justify-content: space-around;
	`
	const ProductName = styled.span`

	`
	const ProductDescription = styled.span`

	`
	const ProductPrice = styled.span`

	`
	const PriceDetail = styled.div`
	flex: 1;
	display:flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-around;
	`
	const ProductAmountContainer = styled.div`
	display: flex;
	justify-content: space-around;
	align-items: center;
	`
	const Quantity = styled.div`

	`
	const ProductAmount = styled.div`
	font-size: 20px;
	margin: 0 20px;
	`
	const Hr = styled.hr`
	background-color: black;
	border: none;
	height: 1px; 
	`


	return(
		<Fragment>
			
			<Product>
				<ProductDetail>
					
					<Image src={imgSrc}/>
					<Details>
						<ProductName>{name}</ProductName>
						<ProductDescription>Description: {description}</ProductDescription>
						<ProductPrice>Price: Php{price}</ProductPrice>
					</Details>
				</ProductDetail>
				<PriceDetail>
					<Quantity>Qty:</Quantity>
					<ProductAmountContainer>
						<Button onClick={() => subtractQuantity()} className="text-center" variant="info"><GrSubtract/></Button>

						<ProductAmount>{quantity}</ProductAmount>

						
						<Button onClick={() => addQuantity()} className="text-center" variant="info"><GrAdd/></Button>
					</ProductAmountContainer>
					<ProductPrice>Php {totalAmount}</ProductPrice>
				</PriceDetail>
			</Product>
			<Hr/>
			
		</Fragment>
		
		





		)
}