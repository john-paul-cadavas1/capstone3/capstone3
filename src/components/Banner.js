import { Carousel } from 'react-bootstrap';
import styled from 'styled-components';







export default function Banner() {


	return(
		<Carousel className="d-flex">
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://onepiece-store.com/wp-content/uploads/2021/06/13.png"
		      alt="First slide"
		    />
		  </Carousel.Item>
		  
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://www.fromjapan.co.jp/lp/image/en/onepiece/onepiece_banner.jpg"
		      alt="Second slide"
		    />
		  </Carousel.Item>
		  
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="https://www.boxgk.com/wp-content/uploads/2021/08/O1CN01ySNyYh2J013oEooR1_2206541839358.jpg"
		      alt="Third slide"
		    />
		  </Carousel.Item>
		</Carousel>




		)
}