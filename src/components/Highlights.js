import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights () {

	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 text-center">
				  <Card.Body>
				    <Card.Title>Quiality Products</Card.Title>
				    <Card.Text>
				      Products are 100% authentic and all from Japan.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 text-center">
				  <Card.Body>
				    <Card.Title>Fast Delivery</Card.Title>
				    <Card.Text>
				      Items are immediately shipped after order.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 text-center">
				  <Card.Body>
				    <Card.Title>Accepts COD</Card.Title>
				    <Card.Text>
				      Our store accepts Cash on Delivery type of payment.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

		)
}
